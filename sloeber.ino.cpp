#ifdef __IN_ECLIPSE__
//This is a automatic generated file
//Please do not modify this file
//If you touch this file your change will be overwritten during the next build
//This file has been generated on 2019-02-20 01:39:56

#include "Arduino.h"
#include "Arduino.h"
#include <avr/sleep.h>
#include "State.h"

void setup() ;
void loop() ;
inline void notify();
inline void lightAutoDim(uint8_t dim) ;
void dim() ;
inline void sleep() ;
void interrupted() ;
void updateLight() ;
void toggleLight(State state) ;
inline void getButtonActions() ;
inline void resetBtnActions() ;
void debugBlink() ;

#include "FancyLight.ino"


#endif
