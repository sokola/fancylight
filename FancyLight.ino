#include "Arduino.h"
#include <avr/sleep.h>
#include "State.h"

// 1 or 2 buttons. If definition is missing then 1-buttoned version assumed.
#define BUTTONS 1

// Enables regulated brightness. Comment out definition to disable it.
#define DIMMABLE

// Timeout in sec to auto off the light. 0 to disable auto off feature.
#define AUTO_OFF_SEC 100

// Time of extended auto off. 0 to disable it. User doubleclicks to extend time.
#define AUTO_OFF_EXTEND 900

// Minimal PWM value for dimmed light.
#define DIM_MIN 15

// Pin for BUTTON 1
#define BUTTON1 2
#if BUTTONS == 2

// Pin for BUTTON 2
#define BUTTON2 0
#endif

// Pin for light. Has to be a PWM pin.
#define LIGHT 11

// Inverts light current flow. Comment out the definition to disable. If non inverted, light has to be connected against ground. If inverted, connect against VCC.
#define INVERT_LIGHT

// Max millis time between button press to detect it as doubleclicks
#define DOUBLECLICK_TRESHOLD_MILLIS 500

// Minimal millis time while button is pressed to detect it as long press.
#define LONG_PRESS_TRESHOLD_MILLIS 700

// Time in which light power transitions from old to new value. Applies to ON - OFF and other actions.
#define FADE_DURATION 800

// How many milliseconds to change power by 1 when holding the button
#define DIM_COEFF 10

// For how long the light goes in low power before switch off entirely
#define AUTO_OFF_DIM_SECONDS 10

// If going auto off, how dark will light go.
#define AUTO_OFF_DIM_PERCENT 30

#define DEBUG_PIN 13

// controls ON / OFF / half state
byte shinePercent;
// current PWM value, changes during fade
byte currentPower;
// what PWM value is light going to be, regarding ON / OFF / half state
byte targetPower;
// what PWM value is when light is ON. Gets regulated by dimmer.
byte onPower = 100;

// help variable for fader
byte oldPower = 0;


enum ButtonAction {
	NONE, PUSH, DOUBLECLICK, HOLD
};

enum Dim {
	DOWN, UP
};

ButtonAction buttons[2];

Dim direction = DOWN;
State nextButtonAction = OFF;

volatile bool buttonPressed = false;
unsigned long lastChangeTime;
unsigned long lastButtonPressTime;

unsigned int secondsFromLastPress;

unsigned int secondsToAutoOff = AUTO_OFF_SEC;

void setup() {
	shinePercent = 0;
	currentPower = 0;
	targetPower = 0;
	pinMode(BUTTON1, INPUT_PULLUP);
#if BUTTONS == 2
	pinMode(BUTTON2, INPUT_PULLUP);
#endif
	pinMode(LIGHT, OUTPUT);
	pinMode(DEBUG_PIN, OUTPUT);

	sleep_enable()
	;
	set_sleep_mode(SLEEP_MODE_PWR_DOWN);
	cli();
	sleep_bod_disable()
	;
	attachInterrupt(digitalPinToInterrupt(BUTTON1), interrupted, FALLING);
#if BUTTONS == 2
	attachInterrupt(digitalPinToInterrupt(BUTTON2), interrupted, FALLING);
#endif
	sei();
	resetBtnActions();
	lastChangeTime = millis();
}

void loop() {
	if (buttonPressed) {
		getButtonActions();
		lastButtonPressTime = millis();

		switch (buttons[0]) {
		case PUSH:
			toggleLight(nextButtonAction);
			if (nextButtonAction == ON) {
				nextButtonAction = OFF;
			} else {
				nextButtonAction = ON;
			}
			secondsToAutoOff = AUTO_OFF_SEC;
			break;

		case HOLD:
			dim();
			break;

		case DOUBLECLICK:
			notify();
			toggleLight(ON);
			nextButtonAction = OFF;
			secondsToAutoOff = AUTO_OFF_EXTEND;
			break;
		}
		resetBtnActions();
	}
	updateLight();

	if (shinePercent == 0 && currentPower == 0) {
		sleep();
	}

	if (shinePercent > 0) {
		secondsFromLastPress = (millis() - lastButtonPressTime) / 1000;
	} else {
		secondsFromLastPress = 0;
	}

	if (secondsToAutoOff > 0 && secondsFromLastPress >= secondsToAutoOff) {
		if (shinePercent == 100 && secondsFromLastPress <= secondsToAutoOff + AUTO_OFF_DIM_SECONDS) {
			lightAutoDim(AUTO_OFF_DIM_PERCENT);
		} else if (shinePercent == AUTO_OFF_DIM_PERCENT && secondsFromLastPress > secondsToAutoOff + AUTO_OFF_DIM_SECONDS){
			lightAutoDim(0);
		}
	}

}

#define NOTIFY_DELAY 60

inline void notify(){
#ifdef INVERT_LIGHT
	analogWrite(LIGHT, 255 - currentPower / 4);
	delay(NOTIFY_DELAY);
	analogWrite(LIGHT, 255 - currentPower);
#else
	analogWrite(LIGHT, currentPower / 4);
	delay(NOTIFY_DELAY);
	analogWrite(LIGHT, currentPower);

#endif
}

inline void lightAutoDim(uint8_t dim) {
	oldPower = currentPower;
	lastChangeTime = millis();
	nextButtonAction = ON;
	shinePercent = dim;
}

void dim() {
	unsigned long dimStart = millis();
	long oldValue = onPower;
	while (!digitalRead(BUTTON1)) {
		long diff = (millis() - dimStart) / DIM_COEFF;
		if (direction == DOWN) {
			if (oldValue >= DIM_MIN + diff) {
				onPower = oldValue - diff;
			}
		} else {
			if (oldValue <= 255 - diff) {
				onPower = oldValue + diff;
			}
		}
		updateLight();
	}

	if (direction == DOWN) {
		direction = UP;
	} else {
		direction = DOWN;
	}
}

inline void sleep() {
	sleep_cpu()
	;
}

void interrupted() {
	buttonPressed = true;
}

void updateLight() {
	targetPower = (short)(onPower * shinePercent) / 100;

	if (millis() - lastChangeTime <= FADE_DURATION) {
		currentPower = (long)(millis() - lastChangeTime) * ((long)targetPower - (long)oldPower) / (long)FADE_DURATION + (long)oldPower;
	} else {
		currentPower = targetPower;
	}

#ifdef INVERT_LIGHT
	analogWrite(LIGHT, 255 - currentPower);
#else
	analogWrite(LIGHT, currentPower);
#endif
}

void toggleLight(State state) {
	lastChangeTime = millis();
	oldPower = currentPower;
	if (state == ON) {
		shinePercent = 100;
	} else {
		shinePercent = 0;
	}
}

inline void getButtonActions() {
	const word time = max(DOUBLECLICK_TRESHOLD_MILLIS, LONG_PRESS_TRESHOLD_MILLIS);
	const byte sampleTime = 50;
	const word n = time / sampleTime;
	const byte readsForSample = 8;
	const byte timeBetweenReads = sampleTime / readsForSample;
	byte samples[n];
	byte sampleReads[n];
	bool sampleValues[n];
	// reset array elements to 0
	memset(samples, 0, n * sizeof(*samples));
	memset(sampleReads, 0, n * sizeof(*sampleReads));

#ifndef BUTTON2
	byte button = BUTTON1;
#else
	//	todo determine which button is pressed
#endif

	// for each sample, read multiple times the button state and remember
	// how many times it was pressed and how many reads in total we performed.
	unsigned long lastMillis = millis();
	for (byte i = 0; i < n; i++) {
		while (millis() < lastMillis + i * sampleTime) {
			byte read = !digitalRead(button);
			samples[i] += read;
			sampleReads[i]++;
			if (timeBetweenReads != 0) {
				delay(timeBetweenReads);
			} else {
				delay(1);
			}
		}
	}
	// if in sample time, count if button was mostly pressed, or not
	for (byte i = 0; i < n; i++) {
		sampleValues[i] = samples[i] * 2 / sampleReads[i];
	}
	// now decide from sequence of sample booleans, what kind of button press it was
	bool doubleClick = false;
	// if there is false sample followed by true sample, it was a double click
	for (byte i = 0; i < n - 1; i++) {
		doubleClick |= !sampleValues[i] && sampleValues[i + 1];
	}
	bool lastSample = sampleValues[n - 1];

	ButtonAction action;
	if (doubleClick) {
		action = DOUBLECLICK;
	} else if (lastSample) {
		action = HOLD;

	} else {
		action = PUSH;
	}
	// update the global state
	if (button == BUTTON1) {
		buttons[0] = action;
	} else {
		buttons[1] = action;
	}
}

inline void resetBtnActions() {
	buttonPressed = false;
	buttons[0] = NONE;
	buttons[1] = NONE;
}

void debugBlink() {
	digitalWrite(DEBUG_PIN, HIGH);
	delay(100);
	digitalWrite(DEBUG_PIN, LOW);
}
